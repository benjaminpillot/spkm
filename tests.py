# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""

import random

import gdal

from matplotlib import pyplot as plt
from sklearn.cluster import KMeans, SpectralClustering

from spkm.kmeans import SPKMeans

# img = gdal.Open("/home/benjamin/Documents/PRO/PRODUITS/FOTO_RGB/SENTINEL/clip_method=block_wsize"
#                 "=19_dc=True_rgb.tif")
# img = gdal.Open("/home/benjamin/Documents/PRO/PRODUITS/FOTO_RGB/SENTINEL"
#                 "/T30NTP_20201113T104311_B03_method=block_wsize=19_dc=True_rgb.tif")
img = "/home/benjamin/Documents/PRO/PRODUITS/FOTO_RGB/SENTINEL/T23LKC_20201006T132241_B03_method" \
      "=block_wsize=19_dc=True_rgb.tif"

# _kmeans(samples, nb_clusters, cluster_size, metric, clusters, centers, method='hartigan',
#         nb_iterations=10, tol=.1)

nb_clusters = 5
inertia = []
test = SPKMeans(nb_clusters, max_iter=50,
                initializer="kmeans++", method="spatial-hartigan",
                spatial_weight=0.6)
init1 = test.initialize(img, random_state=random.randint(0, 2**256), no_data_value=-999,
                        scaled=False)  #.run()
# init1.save_labels()
# plt.figure(1)
# plt.imshow(init1.labels)

# Test sklearn
print("SKLEARN...\n")
samples = init1.samples
labels = SpectralClustering(n_clusters=nb_clusters).fit_predict(samples)
# labels = KMeans(n_clusters=nb_clusters).fit_predict(samples)
labels = labels.reshape(578, 578)

plt.figure(2)
plt.imshow(labels)
plt.show()
