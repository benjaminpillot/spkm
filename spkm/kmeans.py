# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os

import gdal
import numpy as np
from copy import copy

from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer, \
    random_center_initializer

from spkm.core import _kmeans
from spkm.utils import cluster_variance, get_centers, get_labels, size_of_clusters, \
    multi_spectral_to_sample, normalized_mse, spatial_inertia, scale_range
from spkm.metrics import euclidean_distance


class SPKMeans:

    labels = None
    cluster_centers = None
    initial_centers = None
    cluster_size = None
    samples = None
    gdal_dataset = None
    image_width = None
    image_height = None
    random_state = None
    no_data = None

    def __init__(self, nb_clusters, metric=euclidean_distance, method="hartigan",
                 initializer="kmeans++", spatial_weight=0.5, max_iter=100, tolerance=1e-6):
        """ KMeans constructor

        Parameters
        ----------
        nb_clusters: int
            Number of clusters

        metric: function, default=euclidean_distance
            Metric for which distance between points
            and cluster centroids must be calculated

        method: str, default='hartigan'
            Algorithm used to compute k-means

            'hartigan' implements the original Hartigan heuristic [1]

            'spatial-hartigan' implements the original Hartigan heuristic
            with the spatial modification proposed by Theiler et al. [2]

            'lloyd' implements the Lloyd algorithm  # TODO: implement Lloyd's algorithm

        initializer:str, default="kmeans++"
            Initialization method ('kmeans++', 'random')

        spatial_weight: float, default=0.5
            When spatial K-means are computed, weight of (dis)contiguity

        max_iter: int, default=100
            Maximum number of iterations before stopping the algorithm

        tolerance: float, default=1e-3
            Minimum stopping criterion tolerance from one iteration to another

        References
        ----------
        [1] John A. Hartigan, (1975). Clustering Algorithms.
        [2] Theiler, J., & Gisler, G. (1997). A contiguity-enhanced k-means clustering algorithm
            for unsupervised multispectral image segmentation. 3159, 108–118.

        """
        self.nb_clusters = nb_clusters
        self.metric = metric
        self.method = method
        self.initializer = initializer
        self.spatial_weight = spatial_weight
        self.max_iter = max_iter
        self.tolerance = tolerance

    def _initialize_centers(self):
        """ Initialize cluster centers

        Returns
        -------

        """
        if self.no_data is not None:
            samples = self.samples[~self.no_data]
        else:
            samples = self.samples

        if self.initializer == "kmeans++":
            self.initial_centers = \
                kmeans_plusplus_initializer(samples,
                                            self.nb_clusters,
                                            random_state=self.random_state).initialize()
        else:
            self.initial_centers = \
                random_center_initializer(samples,
                                          self.nb_clusters,
                                          random_state=self.random_state).initialize()

    def initialize(self, image, scaled=True, random_state=None, no_data_value=None):
        """ Initialize clusters and centers

        Parameters
        ----------
        image: str
            Path to multi-spectral image
        scaled: bool
            if True, scale each feature of samples (that is each band of image)
        random_state: int, default=None
        no_data_value: float
            No data value

        Returns
        -------
        SPKMeans
            new instance

        """
        # Read image
        self.gdal_dataset = gdal.Open(image)

        # Image shape
        image = self.gdal_dataset.ReadAsArray()
        self.image_width = image.shape[2]
        self.image_height = image.shape[1]

        # Seed random initialization
        self.random_state = random_state

        # Retrieve samples from image and pad no data values
        self.samples = multi_spectral_to_sample(image)
        zero_padding = np.mean(self.samples[~np.any(self.samples == no_data_value, axis=1), :],
                               axis=0)
        self.samples[np.any(self.samples == no_data_value, axis=1), :] = zero_padding
        # self.no_data = np.any(self.samples == no_data_value, axis=1)

        # Scale image
        if scaled:
            self.samples = scale_range(self.samples, -1, 1, self.no_data)

        # Centers + labels + cluster size
        self._initialize_centers()
        self.labels = get_labels(self.samples, self.initial_centers, self.metric)
        # self.labels[self.no_data] = -1
        self.cluster_centers = get_centers(self.samples, self.labels, self.nb_clusters)
        self.cluster_size = size_of_clusters(self.labels, self.nb_clusters)
        self.labels = self.labels.reshape((self.image_height, self.image_width))

        return copy(self)

    def predict(self, image):
        """ Return cluster labels for given array of samples

        Parameters
        ----------
        image: numpy.ndarray

        Returns
        -------

        """
        image_width = image.shape[2]
        image_height = image.shape[1]
        samples = image.reshape((image.shape[0], image_height * image_width)).transpose()

        return get_labels(samples, self.cluster_centers,
                          self.metric).reshape((image_height, image_width))

    def run(self):
        """ Run K-means partitioning

        Returns
        -------

        """
        _kmeans(self.samples,
                self.nb_clusters,
                self.cluster_size,
                self.labels,
                self.cluster_centers,
                self.metric,
                self.method,
                self.spatial_weight,
                self.max_iter,
                self.tolerance)

        return self

    def save_labels(self, gdal_driver="GTiff"):
        """ Save labels to raster file

        Parameters
        ----------
        gdal_driver

        Returns
        -------

        """
        out_dataset = gdal.GetDriverByName(gdal_driver).Create(self.path,
                                                               self.image_width,
                                                               self.image_height,
                                                               1,
                                                               gdal.GetDataTypeByName('Int16'))
        out_dataset.SetGeoTransform(self.gdal_dataset.GetGeoTransform())
        out_dataset.SetProjection(self.gdal_dataset.GetProjection())

        # Write to file
        out_dataset.GetRasterBand(1).WriteArray(self.labels)

        # Close dataset
        out_dataset = None

    @property
    def inertia(self):
        if self.method == "hartigan":
            return normalized_mse(self.samples, self.cluster_centers,
                                  self.labels.ravel(), self.cluster_size, self.metric)
        elif self.method == "spatial-hartigan":
            return spatial_inertia(self.samples, self.cluster_centers,
                                   self.cluster_size, self.labels, self.metric,
                                   self.spatial_weight)

    @property
    def image_name(self):
        if self.gdal_dataset is not None:
            return os.path.splitext(os.path.split(self.gdal_dataset.GetDescription())[1])[0]

    @property
    def path(self):
        if self.gdal_dataset is not None:
            return os.path.join(self.out_dir, f"{self.image_name}_kmeans_nb_clusters="
                                              f"{self.nb_clusters}")

    @property
    def out_dir(self):
        if self.gdal_dataset is not None:
            return os.path.dirname(self.gdal_dataset.GetDescription())
