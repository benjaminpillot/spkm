# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import numpy as np
from numba import njit


def clustering_discontiguity(labels, neighbour_labels):
    """ Compute discontiguity of the whole clustering

    Parameters
    ----------
    labels: numpy.ndarray
    neighbour_labels: list[numpy.ndarray]

    Returns
    -------

    """
    return np.sum(list(map(discontiguity, labels, neighbour_labels))) / labels.size


@njit()
def discontiguity(point_label, neighbour_labels):
    """ Compute discontiguity on given point

    Parameters
    ----------
    point_label: int
    neighbour_labels: numpy.ndarray

    Returns
    -------

    """
    return neighbour_labels[neighbour_labels != point_label].size / neighbour_labels.size


@njit()
def euclidean_distance(point_1, point_2):
    """ Compute Euclidean distance between 2 points

    Parameters
    ----------
    point_1: numpy.ndarray
    point_2: numpy.ndarray

    Returns
    -------

    """
    if point_1.ndim <= 1:
        return np.sqrt(np.sum((point_1 - point_2)**2))
    else:
        return np.sqrt(np.sum((point_1 - point_2) ** 2, axis=1))
