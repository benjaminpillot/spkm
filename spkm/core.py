# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import numpy as np
from numba import njit
from tqdm import tqdm

from spkm.utils import get_adjacent_pixels, delta_of_discontiguity, \
    delta_of_variance, update_centers, total_variance, normalized_mse


def _kmeans(samples, nb_clusters, cluster_size, labels, centers,
            metric, method, spatial_weight, max_iter, tol):
    """ Run K-means algorithm with respect to defined method

    Parameters
    ----------
    samples: numpy.ndarray
    nb_clusters: int
    cluster_size: numpy.ndarray
    metric: function
    labels: numpy.ndarray
    centers: numpy.ndarray
    method: str
        K-means algorithm
    spatial_weight: float
    max_iter: int
    tol: float

    Returns
    -------

    """
    label_id = np.arange(nb_clusters)
    v0 = total_variance(samples, centers, cluster_size, metric)
    nb_samples = samples.shape[0]
    iterator = tqdm(range(max_iter))
    error = 1

    for i in iterator:
        if method == "hartigan":
            new_error = hartigan(samples, labels.ravel(), cluster_size,
                                 centers, metric, label_id, v0, error)
        elif method == "spatial-hartigan":
            new_error = spatial_hartigan(samples, labels, cluster_size,
                                         centers, metric, label_id,
                                         spatial_weight, v0, nb_samples,
                                         error)
        elif method == "lloyd":
            new_error = 1
            pass

        else:
            new_error = 1

        if error - new_error < tol:
            iterator.close()
            break
        else:
            error = new_error


@njit()
def hartigan(samples, labels, cluster_size,
             centers, metric, label_id,
             v0, inertia):
    """ Apply k-means Hartigan original exchange method

    Parameters
    ----------
    samples: numpy.ndarray
        n_samples * m_features array
    labels: numpy.ndarray
        y_size * x_size array
    cluster_size: numpy.ndarray
    centers: numpy.ndarray
    metric: function
    label_id:
    v0:
    inertia:

    Returns
    -------

    """
    for n, sample in enumerate(samples):

        old_label = labels[n]

        delta_v = np.array([delta_of_variance(sample, centers[old_label], centers[new_label],
                                              cluster_size[old_label], cluster_size[new_label],
                                              metric)
                            if new_label != old_label else 0 for new_label in label_id])

        new_label = delta_v.argmin()

        if new_label != old_label:
            centers[old_label], centers[new_label] = \
                update_centers(sample, centers[old_label], centers[new_label],
                               cluster_size[old_label], cluster_size[new_label])
            cluster_size[old_label] -= 1
            cluster_size[new_label] += 1
            labels[n] = new_label
            inertia += delta_v[new_label] / v0

    return inertia


# TODO: implements LLoyd's algorithm
def lloyd():
    pass


@njit()
def spatial_hartigan(samples, labels, cluster_size, centers,
                     metric, label_id, spatial_weight, v0,
                     nb_samples, inertia):
    """ Apply k-means spatial exchange method

    Error is computed with respect to (dis)contiguity and non(compactness)
    according to the algorithm of Theiler et al. [1]

    Parameters
    ----------
    samples: numpy.ndarray
        n_samples * m_features array
    labels: numpy.ndarray
        2D array corresponding to given image
    cluster_size
    centers
    metric
    label_id
    spatial_weight
    v0: float
        total variance of samples
    nb_samples: int
        total number of samples
    inertia

    References
    ----------
    [1] Theiler, J., & Gisler, G. (1997). A contiguity-enhanced k-means clustering algorithm
        for unsupervised multispectral image segmentation. 3159, 108–118.

    Returns
    -------

    """
    all_neighbours = [p for p in get_adjacent_pixels(labels)]
    flatten_labels = labels.ravel()

    for n, sample in enumerate(samples):

        old_label = flatten_labels[n]
        delta_d = np.array([delta_of_discontiguity(old_label, new_label,
                                                   all_neighbours[n], nb_samples)
                            if new_label != old_label else 0 for new_label in label_id])
        delta_v = np.array([delta_of_variance(sample, centers[old_label], centers[new_label],
                                              cluster_size[old_label], cluster_size[new_label],
                                              metric)
                            if new_label != old_label else 0 for new_label in label_id])

        # ! WARNING ! The following form is not working with Numba...
        # It returns a ZeroDivisionError
        # TODO: fix numba floating-point pitfall
        # delta_e = np.array([spatial_weight * delta_of_discontiguity(old_label, new_label,
        #                                                             all_neighbours[n], nb_samples)
        #                     + (1 - spatial_weight) * delta_of_variance(sample, centers[old_label],
        #                                                                centers[new_label],
        #                                                                cluster_size[old_label],
        #                                                                cluster_size[new_label],
        #                                                                metric) / v0
        #                     if new_label != old_label else 0 for new_label in label_id])

        delta_e = spatial_weight * delta_d + (1 - spatial_weight) * delta_v / v0

        # new_label = np.array(delta_e).argmin()
        new_label = delta_e.argmin()

        if new_label != old_label:
            centers[old_label], centers[new_label] = \
                update_centers(sample, centers[old_label], centers[new_label],
                               cluster_size[old_label], cluster_size[new_label])
            cluster_size[old_label] -= 1
            cluster_size[new_label] += 1
            flatten_labels[n] = new_label
            inertia += delta_e[new_label]

    return inertia
