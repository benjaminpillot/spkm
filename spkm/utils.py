# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import numpy as np
from numba import njit, float64, jit

from spkm.metrics import discontiguity, clustering_discontiguity


@njit()
def argmin(alist):
    """

    Parameters
    ----------
    alist: numba.typed.List

    Returns
    -------

    """

    for n, val in enumerate(alist):
        if val == min(alist):
            return n


@njit()
def get_adjacent_pixels(array, is_8_connected=True, no_data=None):
    """ Return adjacent pixels in 2D array

    Parameters
    ----------
    array
    is_8_connected: bool
        8-connection neighbourhood for adjacent pixels if True, 4-connection otherwise
    no_data: float
        No data value

    Returns
    -------

    """
    for y in range(array.shape[0]):
        for x in range(array.shape[1]):
            yield np.array([array[i, j] for i in (y-1, y, y+1) for j in (x-1, x, x+1)
                            if (0 <= i <= array.shape[0] - 1 and 0 <= j <=
                                array.shape[1] - 1) and (i != y or j != x) and
                            (array[i, j] != no_data)])


def multi_spectral_to_sample(image):
    """ Convert multi spectral (3D) image into m_samples * n_features array

    Parameters
    ----------
    image

    Returns
    -------

    """
    return image.reshape((image.shape[0], image.shape[1] * image.shape[2])).transpose()


def sample_to_multi_spectral(sample, width, height):
    """ Convert sample array into multi spectral image

    Parameters
    ----------
    sample
    width
    height

    Returns
    -------

    """
    if sample.shape[0] != width * height:
        raise ValueError("Invalid width*height size")

    return sample.transpose().reshape(sample.shape[1], height, width)


def scale_range(array, min_, max_, no_data=None, axis=0):
    """ Scale array to range values

    Parameters
    ----------
    array
    min_
    max_
    no_data
    axis

    Returns
    -------

    """
    if no_data is not None:
        return (array - np.min(array[~no_data], axis=axis)) * (max_ - min_) / \
               (np.max(array[~no_data], axis=axis) - np.min(array[~no_data], axis=axis)) + min_
    else:
        return (array - np.min(array, axis=axis)) * (max_ - min_) / \
               (np.max(array, axis=axis) - np.min(array, axis=axis)) + min_


def cluster_variance(samples, centers, labels, metric, *args):
    """ Compute cluster variance according to given metric

    Parameters
    ----------
    samples: numpy.ndarray
        n_samples * n_features array
    centers
    labels
    metric
        numpy-based metric
    args
        extra arguments for metric function (if any)

    Returns
    -------

    """
    return np.sum([np.sum(metric(samples[labels == n, :], center, *args) ** 2)
                   for n, center in enumerate(centers)])


@njit()
def delta_of_discontiguity(old_label, new_label, neighbour_labels, nb_samples):
    """ Compute ΔD (delta of discontiguity) for spatial contiguity

    Compute effect on contiguity from exchange between clusters

    Parameters
    ----------
    old_label
    new_label
    neighbour_labels
    nb_samples

    Returns
    -------

    """
    return 2 * (discontiguity(new_label, neighbour_labels) -
                discontiguity(old_label, neighbour_labels))  # / nb_samples


@njit()
def delta_of_variance(sample, src_center, dest_center,
                      src_numel, dest_numel, metric):
    """ Compute exchange ΔV

    Parameters
    ----------
    sample
    src_center: numpy.ndarray
        center of source cluster
    dest_center: numpy.ndarray
        center of destination cluster
    src_numel: int
        number of elements in source cluster
    dest_numel: int
        number of element in destination cluster
    metric

    Returns
    -------

    """
    var2 = metric(sample, dest_center) ** 2 * dest_numel / (dest_numel + 1)
    var1 = metric(sample, src_center) ** 2 * src_numel / (src_numel - 1)

    return var2 - var1


def get_centers(samples, clusters, nb_clusters):
    """ Compute centers for each corresponding cluster

    Parameters
    ----------
    samples
    clusters
    nb_clusters

    Returns
    -------

    """
    return np.array([samples[clusters == n, :].mean(axis=0)
                     for n in range(nb_clusters)])


def get_labels(samples, centers, metric, *args):
    """ Get cluster label for each sample

    Put each sample in the cluster it belongs to by computing
    the corresponding metric

    Parameters
    ----------
    samples: numpy.ndarray
        numpy array of m_samples * n_features
    centers: numpy.ndarray
    metric: function
        numpy-based metric
    args:
        extra arguments for metric function (if any)

    Returns
    -------

    """
    distance = [metric(samples, center, *args)
                for center in centers]

    return np.argmin(distance, axis=0)


@njit()
def update_centers(sample, src_center, dest_center, src_numel, dest_numel):
    """ Update centers of the clusters in O(1) time

    Centers of clusters are updated after exchange is performed

    Parameters
    ----------
    sample: numpy.ndarray
        sample that is exchanged from old cluster to new cluster
    src_center: numpy.ndarray
        center of source cluster
    dest_center: numpy.ndarray
        center of destination cluster
    src_numel: int
        number of elements in source cluster
    dest_numel: int
        number of elements in destination cluster

    Returns
    -------

    """
    new_src_center = (src_numel * src_center - sample) / (src_numel - 1)
    new_dest_center = (dest_numel * dest_center + sample) / (dest_numel + 1)

    return new_src_center, new_dest_center


def size_of_clusters(clusters, nb_clusters):
    """ Return nb of elements in each cluster

    Parameters
    ----------
    clusters
    nb_clusters

    Returns
    -------

    """
    return np.array([clusters[clusters == n].size
                     for n in range(nb_clusters)])


def total_variance(samples, centers, cluster_size, metric, *args):
    """ Compute total variance of all samples with respect to given metric

    Parameters
    ----------
    samples
    centers
    cluster_size
    metric
    args

    Returns
    -------

    """
    center = np.sum(centers * cluster_size[..., None], axis=0) / cluster_size.sum()

    return np.sum(metric(samples, center, *args) ** 2)


def normalized_mse(samples, centers, labels, cluster_size, metric):
    """ Compute normalized mean squared error

    Parameters
    ----------
    samples
    centers
    labels
    cluster_size
    metric

    Returns
    -------

    """
    return cluster_variance(samples, centers, labels,
                            metric) / total_variance(samples, centers, cluster_size, metric)


def spatial_inertia(samples, centers, cluster_size,
                    labels, metric, spatial_weight):
    """ Compute spatial inertia

    According to Theiler et al. [1], spatial inertia
    is defined as the sum of discontiguity and
    normalized mean squared error.

    Parameters
    ----------
    samples
    centers
    cluster_size
    labels
    metric
    spatial_weight

    References
    ----------
    [1] Theiler, J., & Gisler, G. (1997). A contiguity-enhanced k-means clustering algorithm
        for unsupervised multispectral image segmentation. 3159, 108–118.

    Returns
    -------

    """
    neighbour_labels = [p for p in get_adjacent_pixels(labels)]
    spatial_discontiguity = clustering_discontiguity(labels.ravel(), neighbour_labels)
    mse = normalized_mse(samples, centers, labels.ravel(), cluster_size, metric)

    return spatial_weight * spatial_discontiguity + (1 - spatial_weight) * mse
