"""
======================================================================
A demo of structured Ward hierarchical clustering on an image of coins
======================================================================

Compute the segmentation of a 2D image with Ward hierarchical
clustering. The clustering is spatially constrained in order
for each segmented region to be in one piece.
"""

# Author : Vincent Michel, 2010
#          Alexandre Gramfort, 2011
# License: BSD 3 clause
import cv2
import gdal
# import hdbscan

from spkm.utils import multi_spectral_to_sample, sample_to_multi_spectral

print(__doc__)

from pyrasta.raster import Raster

import time as time

import numpy as np
from scipy.ndimage.filters import gaussian_filter

import matplotlib.pyplot as plt

import skimage
from skimage.transform import rescale

from sklearn.feature_extraction.image import grid_to_graph
from sklearn.cluster import AgglomerativeClustering, KMeans
from sklearn.utils.fixes import parse_version

# these were introduced in skimage-0.14
if parse_version(skimage.__version__) >= parse_version('0.14'):
    rescale_params = {'anti_aliasing': False, 'multichannel': False}
else:
    rescale_params = {}

# #############################################################################
# Generate data
rgb_map = Raster("/home/benjamin/Documents/PRO/PRODUITS/FOTO_RGB/SENTINEL/FOTO_method=block_wsize=19_dc=False_image"
                 "=T16SBC_20210321T163951_B03_rgb.tif")
# rgb_map = Raster("/media/benjamin/storage/FOTO_RGB/DC_FALSE/WINDOW_SIZE_19/FOTO_method"
#                  "=block_wsize=19_dc=False_image=T45TUK_20210526T051651_B03_rgb.tif")
rgb_array = rgb_map.read_array()

# Resize it to 20% of the original size to speed up the processing
# Applying a Gaussian filter for smoothing prior to down-scaling
# reduces aliasing artifacts.
smoothened_array = np.moveaxis(cv2.GaussianBlur(np.moveaxis(rgb_array, 0, -1), (7, 7), 0), -1, 0)
# smoothened_array = np.zeros(rgb_array.shape)
# smoothened_array[0, :, :] = gaussian_filter(rgb_array[0, :, :], sigma=1)
# smoothened_array[1, :, :] = gaussian_filter(rgb_array[1, :, :], sigma=1)
# smoothened_array[2, :, :] = gaussian_filter(rgb_array[2, :, :], sigma=1)
# smoothened_array = rescale(smoothened_array, (1, 0.5, 0.5), mode="reflect",
#                            **rescale_params)

# plt.figure(1)
# plt.imshow(np.moveaxis(rgb_array, 0, -1))
# plt.figure(2)
# plt.imshow(np.moveaxis(smoothened_array, 0, -1))

samples = multi_spectral_to_sample(smoothened_array)

# #############################################################################
# Define the structure A of the data. Pixels connected to their neighbors.
# connectivity = grid_to_graph(smoothened_array.shape[1], smoothened_array.shape[2])

# #############################################################################
# Compute clustering
print("Compute structured hierarchical clustering...")
st = time.time()
n_clusters = 3  # number of regions
n_clusters_k = 2
kmeans = KMeans(n_clusters=n_clusters_k)
# ward = AgglomerativeClustering(n_clusters=n_clusters, linkage='ward',
#                                connectivity=connectivity)
labels = kmeans.fit(samples).labels_
# ward.fit(samples)
# label = np.reshape(ward.labels_, (smoothened_array.shape[1], smoothened_array.shape[2]))
label_kmeans = np.reshape(labels, (smoothened_array.shape[1], smoothened_array.shape[2]))

# plt.figure(3)
# plt.imshow(label)

# Raster.from_array(label, rgb_map.crs, rgb_map.bounds).to_file("data/ward_n_clusters=3.tif")

Raster.from_array(label_kmeans, rgb_map.crs,
                  rgb_map.bounds).to_file(f"data/kmeans_n_clusters="
                                          f"{n_clusters_k}_dc=False_w=19.tif")

# Raster.from_array(smoothened_array, rgb_map.crs, rgb_map.bounds).to_file("data/gaussian_filter.tif")

# print(ward.distances_)

# plt.figure(3)
# plt.imshow(label)
# plt.figure(4)
# plt.imshow(label_kmeans)
# print("Elapsed time: ", time.time() - st)
# print("Number of pixels: ", label.size)
# print("Number of clusters: ", np.unique(label).size)

# #############################################################################
# Plot the results on an image
# plt.figure(figsize=(5, 5))
# plt.imshow(rescaled_coins.squeeze(), cmap=plt.cm.gray)
# for l in range(n_clusters):
#     plt.contour(label == l,
#                 colors=[plt.cm.nipy_spectral(l / float(n_clusters)), ])
# plt.xticks(())
# plt.yticks(())
# plt.show()
