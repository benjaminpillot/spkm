# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
from sklearn.metrics import silhouette_score, v_measure_score, davies_bouldin_score
from sklearn.preprocessing import MinMaxScaler

n_components = 5
X, truth = make_blobs(n_samples=500, centers=n_components,
                      cluster_std=[1.5] * n_components,
                      random_state=42)

scaler = MinMaxScaler()
# X_scaled = scaler.fit_transform(X)
X_scaled = X

inertia = []
km_score = []
km_silhouette = []
vmeasure_score = []
db_score = []
for n in range(n_components + 6):
    kmeans = KMeans(n_clusters=n+1).fit(X_scaled)
    preds = kmeans.predict(X_scaled)
    km_score.append(-kmeans.score(X_scaled))
    if n == 0:
        km_silhouette.append(np.nan)
        vmeasure_score.append(np.nan)
        db_score.append(np.nan)
    else:
        km_silhouette.append(silhouette_score(X_scaled, preds))
        vmeasure_score.append(v_measure_score(truth, preds))
        db_score.append(davies_bouldin_score(X_scaled, preds))
    inertia.append(kmeans.inertia_)

inertia = np.asarray(inertia)
# test = np.diff(inertia) / inertia[0:-1]
# r_score = inertia[2::] / inertia[1:-1] - inertia[1:-1] / inertia[0:-2]

# test = np.concatenate([np.diff(inertia) / inertia[0:-1], [np.nan]])
test = np.concatenate([[0], (inertia[0:-1] - inertia[1::])/inertia[0:-1]])
r_score = np.concatenate([[np.nan], np.diff(test)])

plt.figure(1)
plt.scatter(X[:, 0], X[:, 1])

plt.figure(2)
# plt.plot(km_score)
plt.plot(100 * np.asarray(km_silhouette), color="red")
plt.plot(100 * np.asarray(vmeasure_score), color="black")
plt.plot(100 * np.asarray(db_score), color="green")
plt.plot(100 * test, color="purple")
plt.plot(100 * r_score, color="cyan")

plt.legend(['k-means silhouette', 'V-measure score', 'Davies-Bouldin score',
            'R score'], loc="upper left")

plt.show()
